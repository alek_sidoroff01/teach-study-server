package io.swagger.server.api

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.unmarshalling.FromRequestUnmarshaller
import akka.http.scaladsl.marshalling.ToEntityMarshaller
import io.swagger.server.AkkaHttpHelper._
import io.swagger.server.model.AuthResponse
import io.swagger.server.model.Course
import io.swagger.server.model.CourseInput
import io.swagger.server.model.CourseOutput
import io.swagger.server.model.CourseResponse
import io.swagger.server.model.CoursesListResponse
import io.swagger.server.model.Error
import io.swagger.server.model.ErrorResponse
import io.swagger.server.model.Lesson
import io.swagger.server.model.LessonsResponse
import io.swagger.server.model.LoginCredentials
import io.swagger.server.model.Task
import io.swagger.server.model.TaskList
import io.swagger.server.model.Test
import io.swagger.server.model.TestResult
import io.swagger.server.model.User
import io.swagger.server.model.filter
import io.swagger.server.model.inline_response_200
import io.swagger.server.model.inline_response_200_1

class DefaultApi(
    defaultService: DefaultApiService,
    defaultMarshaller: DefaultApiMarshaller
) {
  import defaultMarshaller._

  lazy val route: Route =
    path() { (courseId) => 
      post {
        
          
            
              
                entity(as[Lesson]){ body =>
                  defaultService.addLesson(body = body, courseId = courseId)
                }
             
           
         
       
      }
    } ~
    path() { 
      get {
        parameters("page".as[Int].?, "per_page".as[Int].?, "filter".as[String].?) { (page, perPage, filter) =>
          
            
              
                
                  defaultService.apiUsersGet(page = page, perPage = perPage, filter = filter)
               
             
           
         
        }
      }
    } ~
    path() { (courseId) => 
      delete {
        
          
            
              
                
                  defaultService.coursesCourseIdDelete(courseId = courseId)
               
             
           
         
       
      }
    } ~
    path() { (courseId) => 
      get {
        
          
            
              
                
                  defaultService.coursesCourseIdGet(courseId = courseId)
               
             
           
         
       
      }
    } ~
    path() { (courseId) => 
      get {
        parameters("page".as[Int].?, "limit".as[Int].?) { (page, limit) =>
          
            
              
                
                  defaultService.coursesCourseIdLessonsGet(courseId = courseId, page = page, limit = limit)
               
             
           
         
        }
      }
    } ~
    path() { (courseId, lessonId) => 
      delete {
        
          
            
              
                
                  defaultService.coursesCourseIdLessonsLessonIdDelete(courseId = courseId, lessonId = lessonId)
               
             
           
         
       
      }
    } ~
    path() { (courseId) => 
      put {
        parameters("course".as[String]) { (course) =>
          
            
              
                
                  defaultService.coursesCourseIdPut(courseId = courseId, course = course)
               
             
           
         
        }
      }
    } ~
    path() { (courseId) => 
      get {
        parameters("page".as[Int].?, "limit".as[Int].?) { (page, limit) =>
          
            
              
                
                  defaultService.coursesCourseIdTasksGet(courseId = courseId, page = page, limit = limit)
               
             
           
         
        }
      }
    } ~
    path() { (courseId, taskId) => 
      get {
        
          
            
              
                
                  defaultService.coursesCourseIdTasksTaskIdGet(courseId = courseId, taskId = taskId)
               
             
           
         
       
      }
    } ~
    path() { (courseId, taskId) => 
      post {
        parameters("answer".as[String]) { (answer) =>
          
            
              
                
                  defaultService.coursesCourseIdTasksTaskIdSubmitPost(courseId = courseId, taskId = taskId, answer = answer)
               
             
           
         
        }
      }
    } ~
    path() { (courseId) => 
      get {
        parameters("page".as[Int].?, "limit".as[Int].?) { (page, limit) =>
          
            
              
                
                  defaultService.coursesCourseIdTestsGet(courseId = courseId, page = page, limit = limit)
               
             
           
         
        }
      }
    } ~
    path() { (courseId, userId) => 
      get {
        
          
            
              
                
                  defaultService.coursesCourseIdUsersUserIdStatsGet(courseId = courseId, userId = userId)
               
             
           
         
       
      }
    } ~
    path() { 
      get {
        parameters("page".as[Int].?, "limit".as[Int].?, "search".as[String].?) { (page, limit, search) =>
          
            
              
                
                  defaultService.coursesGet(page = page, limit = limit, search = search)
               
             
           
         
        }
      }
    } ~
    path() { 
      post {
        
          
            
              
                entity(as[CourseInput]){ body =>
                  defaultService.coursesPost(body = body)
                }
             
           
         
       
      }
    } ~
    path() { (lessonId) => 
      get {
        
          
            
              
                
                  defaultService.lessonsLessonIdGet(lessonId = lessonId)
               
             
           
         
       
      }
    } ~
    path() { (testId) => 
      get {
        
          
            
              
                
                  defaultService.testsTestIdGet(testId = testId)
               
             
           
         
       
      }
    } ~
    path() { (testId) => 
      post {
        
          
            
              
                entity(as[TestResult]){ body =>
                  defaultService.testsTestIdResultsPost(body = body, testId = testId)
                }
             
           
         
       
      }
    } ~
    path() { (courseId, lessonId) => 
      put {
        
          
            
              
                entity(as[Lesson]){ body =>
                  defaultService.updateLesson(body = body, courseId = courseId, lessonId = lessonId)
                }
             
           
         
       
      }
    } ~
    path() { 
      post {
        
          
            
              
                entity(as[LoginCredentials]){ body =>
                  defaultService.usersLoginPost(body = body)
                }
             
           
         
       
      }
    } ~
    path() { 
      post {
        
          
            
              
                entity(as[User]){ body =>
                  defaultService.usersRegisterPost(body = body)
                }
             
           
         
       
      }
    } ~
    path() { (userId) => 
      get {
        
          
            
              
                
                  defaultService.usersUserIdGet(userId = userId)
               
             
           
         
       
      }
    }
}

trait DefaultApiService {

  def addLesson201(responseLesson: Lesson)(implicit toEntityMarshallerLesson: ToEntityMarshaller[Lesson]): Route =
    complete((201, responseLesson))
  def addLesson404: Route =
    complete((404, "Курс с указанным course_id не найден"))
  def addLesson500: Route =
    complete((500, "Внутренняя ошибка сервера"))
  /**
   * Code: 201, Message: Успешно создан новый урок, DataType: Lesson
   * Code: 404, Message: Курс с указанным course_id не найден
   * Code: 500, Message: Внутренняя ошибка сервера
   */
  def addLesson(body: Lesson, courseId: Int)
      (implicit toEntityMarshallerLesson: ToEntityMarshaller[Lesson]): Route

  def apiUsersGet200(responseUserarray: List[User])(implicit toEntityMarshallerUserarray: ToEntityMarshaller[List[User]]): Route =
    complete((200, responseUserarray))
  def apiUsersGet500(responseError: Error)(implicit toEntityMarshallerError: ToEntityMarshaller[Error]): Route =
    complete((500, responseError))
  /**
   * Code: 200, Message: Список пользователей, DataType: List[User]
   * Code: 500, Message: Внутренняя ошибка сервера., DataType: Error
   */
  def apiUsersGet(page: Option[Int], perPage: Option[Int], filter: Option[String])
      (implicit toEntityMarshallerUserarray: ToEntityMarshaller[List[User]], toEntityMarshallerError: ToEntityMarshaller[Error]): Route

  def coursesCourseIdDelete204: Route =
    complete((204, "Курс удален успешно."))
  def coursesCourseIdDelete404(responseError: Error)(implicit toEntityMarshallerError: ToEntityMarshaller[Error]): Route =
    complete((404, responseError))
  def coursesCourseIdDelete500(responseError: Error)(implicit toEntityMarshallerError: ToEntityMarshaller[Error]): Route =
    complete((500, responseError))
  /**
   * Code: 204, Message: Курс удален успешно.
   * Code: 404, Message: Курс не найден., DataType: Error
   * Code: 500, Message: Внутренняя ошибка сервера., DataType: Error
   */
  def coursesCourseIdDelete(courseId: Int)
      (implicit toEntityMarshallerError: ToEntityMarshaller[Error], toEntityMarshallerError: ToEntityMarshaller[Error]): Route

  def coursesCourseIdGet200(responseCourseResponse: CourseResponse)(implicit toEntityMarshallerCourseResponse: ToEntityMarshaller[CourseResponse]): Route =
    complete((200, responseCourseResponse))
  def coursesCourseIdGet401: Route =
    complete((401, "Необходима авторизация"))
  def coursesCourseIdGet404: Route =
    complete((404, "Курс не найден"))
  /**
   * Code: 200, Message: Информация о курсе успешно получена, DataType: CourseResponse
   * Code: 401, Message: Необходима авторизация
   * Code: 404, Message: Курс не найден
   */
  def coursesCourseIdGet(courseId: Int)
      (implicit toEntityMarshallerCourseResponse: ToEntityMarshaller[CourseResponse]): Route

  def coursesCourseIdLessonsGet200(responseLessonsResponse: LessonsResponse)(implicit toEntityMarshallerLessonsResponse: ToEntityMarshaller[LessonsResponse]): Route =
    complete((200, responseLessonsResponse))
  def coursesCourseIdLessonsGet401: Route =
    complete((401, "Необходима авторизация"))
  def coursesCourseIdLessonsGet404: Route =
    complete((404, "Курс не найден"))
  /**
   * Code: 200, Message: Список уроков успешно получен, DataType: LessonsResponse
   * Code: 401, Message: Необходима авторизация
   * Code: 404, Message: Курс не найден
   */
  def coursesCourseIdLessonsGet(courseId: Int, page: Option[Int], limit: Option[Int])
      (implicit toEntityMarshallerLessonsResponse: ToEntityMarshaller[LessonsResponse]): Route

  def coursesCourseIdLessonsLessonIdDelete204: Route =
    complete((204, "Успешное удаление урока"))
  def coursesCourseIdLessonsLessonIdDelete500(responseError: Error)(implicit toEntityMarshallerError: ToEntityMarshaller[Error]): Route =
    complete((500, responseError))
  /**
   * Code: 204, Message: Успешное удаление урока
   * Code: 500, Message: Внутренняя ошибка сервера., DataType: Error
   */
  def coursesCourseIdLessonsLessonIdDelete(courseId: Int, lessonId: Int)
      (implicit toEntityMarshallerError: ToEntityMarshaller[Error]): Route

  def coursesCourseIdPut200(responseCourse: Course)(implicit toEntityMarshallerCourse: ToEntityMarshaller[Course]): Route =
    complete((200, responseCourse))
  def coursesCourseIdPut400: Route =
    complete((400, "Некорректный запрос"))
  def coursesCourseIdPut404: Route =
    complete((404, "Курс не найден"))
  def coursesCourseIdPut500: Route =
    complete((500, "Ошибка сервера"))
  /**
   * Code: 200, Message: Информация о курсе успешно обновлена, DataType: Course
   * Code: 400, Message: Некорректный запрос
   * Code: 404, Message: Курс не найден
   * Code: 500, Message: Ошибка сервера
   */
  def coursesCourseIdPut(courseId: Int, course: String)
      (implicit toEntityMarshallerCourse: ToEntityMarshaller[Course]): Route

  def coursesCourseIdTasksGet200(responseTaskList: TaskList)(implicit toEntityMarshallerTaskList: ToEntityMarshaller[TaskList]): Route =
    complete((200, responseTaskList))
  def coursesCourseIdTasksGet404: Route =
    complete((404, "Курс не найден"))
  /**
   * Code: 200, Message: Список заданий успешно получен, DataType: TaskList
   * Code: 404, Message: Курс не найден
   */
  def coursesCourseIdTasksGet(courseId: Int, page: Option[Int], limit: Option[Int])
      (implicit toEntityMarshallerTaskList: ToEntityMarshaller[TaskList]): Route

  def coursesCourseIdTasksTaskIdGet200(responseTask: Task)(implicit toEntityMarshallerTask: ToEntityMarshaller[Task]): Route =
    complete((200, responseTask))
  def coursesCourseIdTasksTaskIdGet404: Route =
    complete((404, "Задание не найдено"))
  /**
   * Code: 200, Message: Информация о задании успешно получена, DataType: Task
   * Code: 404, Message: Задание не найдено
   */
  def coursesCourseIdTasksTaskIdGet(courseId: Int, taskId: Int)
      (implicit toEntityMarshallerTask: ToEntityMarshaller[Task]): Route

  def coursesCourseIdTasksTaskIdSubmitPost200: Route =
    complete((200, "Задание успешно отправлено"))
  def coursesCourseIdTasksTaskIdSubmitPost404: Route =
    complete((404, "Задание не найдено"))
  /**
   * Code: 200, Message: Задание успешно отправлено
   * Code: 404, Message: Задание не найдено
   */
  def coursesCourseIdTasksTaskIdSubmitPost(courseId: Int, taskId: Int, answer: String): Route

  def coursesCourseIdTestsGet200(responseinline_response_200: inline_response_200)(implicit toEntityMarshallerinline_response_200: ToEntityMarshaller[inline_response_200]): Route =
    complete((200, responseinline_response_200))
  def coursesCourseIdTestsGet401: Route =
    complete((401, "Необходима авторизация"))
  def coursesCourseIdTestsGet404: Route =
    complete((404, "Курс не найден"))
  /**
   * Code: 200, Message: Список тестов успешно получен, DataType: inline_response_200
   * Code: 401, Message: Необходима авторизация
   * Code: 404, Message: Курс не найден
   */
  def coursesCourseIdTestsGet(courseId: Int, page: Option[Int], limit: Option[Int])
      (implicit toEntityMarshallerinline_response_200: ToEntityMarshaller[inline_response_200]): Route

  def coursesCourseIdUsersUserIdStatsGet200(responseinline_response_200_1: inline_response_200_1)(implicit toEntityMarshallerinline_response_200_1: ToEntityMarshaller[inline_response_200_1]): Route =
    complete((200, responseinline_response_200_1))
  def coursesCourseIdUsersUserIdStatsGet404: Route =
    complete((404, "Курс или пользователь не найден"))
  /**
   * Code: 200, Message: Статистика по прохождению курса получена успешно, DataType: inline_response_200_1
   * Code: 404, Message: Курс или пользователь не найден
   */
  def coursesCourseIdUsersUserIdStatsGet(courseId: Int, userId: Int)
      (implicit toEntityMarshallerinline_response_200_1: ToEntityMarshaller[inline_response_200_1]): Route

  def coursesGet200(responseCoursesListResponse: CoursesListResponse)(implicit toEntityMarshallerCoursesListResponse: ToEntityMarshaller[CoursesListResponse]): Route =
    complete((200, responseCoursesListResponse))
  def coursesGet401: Route =
    complete((401, "Необходима авторизация"))
  /**
   * Code: 200, Message: Список курсов успешно получен, DataType: CoursesListResponse
   * Code: 401, Message: Необходима авторизация
   */
  def coursesGet(page: Option[Int], limit: Option[Int], search: Option[String])
      (implicit toEntityMarshallerCoursesListResponse: ToEntityMarshaller[CoursesListResponse]): Route

  def coursesPost201(responseCourseOutput: CourseOutput)(implicit toEntityMarshallerCourseOutput: ToEntityMarshaller[CourseOutput]): Route =
    complete((201, responseCourseOutput))
  def coursesPost400(responseErrorResponse: ErrorResponse)(implicit toEntityMarshallerErrorResponse: ToEntityMarshaller[ErrorResponse]): Route =
    complete((400, responseErrorResponse))
  def coursesPost500(responseError: Error)(implicit toEntityMarshallerError: ToEntityMarshaller[Error]): Route =
    complete((500, responseError))
  /**
   * Code: 201, Message: Курс успешно создан, DataType: CourseOutput
   * Code: 400, Message: Некорректный запрос, DataType: ErrorResponse
   * Code: 500, Message: Внутренняя ошибка сервера., DataType: Error
   */
  def coursesPost(body: CourseInput)
      (implicit toEntityMarshallerCourseOutput: ToEntityMarshaller[CourseOutput], toEntityMarshallerErrorResponse: ToEntityMarshaller[ErrorResponse], toEntityMarshallerError: ToEntityMarshaller[Error]): Route

  def lessonsLessonIdGet200(responseLesson: Lesson)(implicit toEntityMarshallerLesson: ToEntityMarshaller[Lesson]): Route =
    complete((200, responseLesson))
  def lessonsLessonIdGet401: Route =
    complete((401, "Необходима авторизация"))
  def lessonsLessonIdGet404: Route =
    complete((404, "Урок не найден"))
  /**
   * Code: 200, Message: Информация об уроке успешно получена, DataType: Lesson
   * Code: 401, Message: Необходима авторизация
   * Code: 404, Message: Урок не найден
   */
  def lessonsLessonIdGet(lessonId: Int)
      (implicit toEntityMarshallerLesson: ToEntityMarshaller[Lesson]): Route

  def testsTestIdGet200(responseTest: Test)(implicit toEntityMarshallerTest: ToEntityMarshaller[Test]): Route =
    complete((200, responseTest))
  def testsTestIdGet401: Route =
    complete((401, "Необходима авторизация"))
  def testsTestIdGet404: Route =
    complete((404, "Тест не найден"))
  /**
   * Code: 200, Message: Информация о тесте успешно получена, DataType: Test
   * Code: 401, Message: Необходима авторизация
   * Code: 404, Message: Тест не найден
   */
  def testsTestIdGet(testId: Int)
      (implicit toEntityMarshallerTest: ToEntityMarshaller[Test]): Route

  def testsTestIdResultsPost200: Route =
    complete((200, "Результаты тестирования успешно сохранены"))
  def testsTestIdResultsPost401: Route =
    complete((401, "Необходима авторизация"))
  def testsTestIdResultsPost404: Route =
    complete((404, "Тест не найден"))
  /**
   * Code: 200, Message: Результаты тестирования успешно сохранены
   * Code: 401, Message: Необходима авторизация
   * Code: 404, Message: Тест не найден
   */
  def testsTestIdResultsPost(body: TestResult, testId: Int): Route

  def updateLesson200(responseLesson: Lesson)(implicit toEntityMarshallerLesson: ToEntityMarshaller[Lesson]): Route =
    complete((200, responseLesson))
  def updateLesson404: Route =
    complete((404, "Урок с указанным lesson_id не найден"))
  def updateLesson500: Route =
    complete((500, "Внутренняя ошибка сервера"))
  /**
   * Code: 200, Message: Успешно изменены данные урока, DataType: Lesson
   * Code: 404, Message: Урок с указанным lesson_id не найден
   * Code: 500, Message: Внутренняя ошибка сервера
   */
  def updateLesson(body: Lesson, courseId: Int, lessonId: Int)
      (implicit toEntityMarshallerLesson: ToEntityMarshaller[Lesson]): Route

  def usersLoginPost200(responseAuthResponse: AuthResponse)(implicit toEntityMarshallerAuthResponse: ToEntityMarshaller[AuthResponse]): Route =
    complete((200, responseAuthResponse))
  def usersLoginPost401: Route =
    complete((401, "Ошибка аутентификации"))
  /**
   * Code: 200, Message: Аутентификация успешна, DataType: AuthResponse
   * Code: 401, Message: Ошибка аутентификации
   */
  def usersLoginPost(body: LoginCredentials)
      (implicit toEntityMarshallerAuthResponse: ToEntityMarshaller[AuthResponse]): Route

  def usersRegisterPost201(responseUser: User)(implicit toEntityMarshallerUser: ToEntityMarshaller[User]): Route =
    complete((201, responseUser))
  def usersRegisterPost400: Route =
    complete((400, "Ошибка валидации данных пользователя"))
  def usersRegisterPost409: Route =
    complete((409, "Пользователь с таким именем уже существует"))
  /**
   * Code: 201, Message: Успешная регистрация нового пользователя, DataType: User
   * Code: 400, Message: Ошибка валидации данных пользователя
   * Code: 409, Message: Пользователь с таким именем уже существует
   */
  def usersRegisterPost(body: User)
      (implicit toEntityMarshallerUser: ToEntityMarshaller[User]): Route

  def usersUserIdGet200(responseUser: User)(implicit toEntityMarshallerUser: ToEntityMarshaller[User]): Route =
    complete((200, responseUser))
  def usersUserIdGet404(responseError: Error)(implicit toEntityMarshallerError: ToEntityMarshaller[Error]): Route =
    complete((404, responseError))
  /**
   * Code: 200, Message: Успешный ответ, DataType: User
   * Code: 404, Message: Пользователь не найден, DataType: Error
   */
  def usersUserIdGet(userId: Int)
      (implicit toEntityMarshallerUser: ToEntityMarshaller[User], toEntityMarshallerError: ToEntityMarshaller[Error]): Route

}

trait DefaultApiMarshaller {
  implicit def fromRequestUnmarshallerCourseInput: FromRequestUnmarshaller[CourseInput]

  implicit def fromRequestUnmarshallerLoginCredentials: FromRequestUnmarshaller[LoginCredentials]

  implicit def fromRequestUnmarshallerUser: FromRequestUnmarshaller[User]

  implicit def fromRequestUnmarshallerTestResult: FromRequestUnmarshaller[TestResult]

  implicit def fromRequestUnmarshallerLesson: FromRequestUnmarshaller[Lesson]


  implicit def toEntityMarshallerLesson: ToEntityMarshaller[Lesson]

  implicit def toEntityMarshallerUserarray: ToEntityMarshaller[List[User]]

  implicit def toEntityMarshallerError: ToEntityMarshaller[Error]

  implicit def toEntityMarshallerError: ToEntityMarshaller[Error]

  implicit def toEntityMarshallerError: ToEntityMarshaller[Error]

  implicit def toEntityMarshallerCourseResponse: ToEntityMarshaller[CourseResponse]

  implicit def toEntityMarshallerLessonsResponse: ToEntityMarshaller[LessonsResponse]

  implicit def toEntityMarshallerError: ToEntityMarshaller[Error]

  implicit def toEntityMarshallerCourse: ToEntityMarshaller[Course]

  implicit def toEntityMarshallerTaskList: ToEntityMarshaller[TaskList]

  implicit def toEntityMarshallerTask: ToEntityMarshaller[Task]

  implicit def toEntityMarshallerinline_response_200: ToEntityMarshaller[inline_response_200]

  implicit def toEntityMarshallerinline_response_200_1: ToEntityMarshaller[inline_response_200_1]

  implicit def toEntityMarshallerCoursesListResponse: ToEntityMarshaller[CoursesListResponse]

  implicit def toEntityMarshallerCourseOutput: ToEntityMarshaller[CourseOutput]

  implicit def toEntityMarshallerErrorResponse: ToEntityMarshaller[ErrorResponse]

  implicit def toEntityMarshallerError: ToEntityMarshaller[Error]

  implicit def toEntityMarshallerLesson: ToEntityMarshaller[Lesson]

  implicit def toEntityMarshallerTest: ToEntityMarshaller[Test]

  implicit def toEntityMarshallerLesson: ToEntityMarshaller[Lesson]

  implicit def toEntityMarshallerAuthResponse: ToEntityMarshaller[AuthResponse]

  implicit def toEntityMarshallerUser: ToEntityMarshaller[User]

  implicit def toEntityMarshallerUser: ToEntityMarshaller[User]

  implicit def toEntityMarshallerError: ToEntityMarshaller[Error]

}

