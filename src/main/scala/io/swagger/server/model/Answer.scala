package io.swagger.server.model


/**
 * @param question_id Идентификатор вопроса
 * @param choice_id Идентификатор выбранного ответа
 */
case class Answer (
  question_id: Int,
  choice_id: Int
)

