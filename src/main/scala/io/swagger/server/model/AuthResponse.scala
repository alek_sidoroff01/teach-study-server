package io.swagger.server.model


/**
 * @param token Токен аутентификации
 */
case class AuthResponse (
  token: String
)

