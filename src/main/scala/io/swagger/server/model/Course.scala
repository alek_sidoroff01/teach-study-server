package io.swagger.server.model

import java.math.BigDecimal

/**
 * @param id Идентификатор курса
 * @param title Название курса
 * @param description Описание курса
 * @param image URL изображения для курса
 * @param price Цена курса
 * @param duration Длительность курса
 */
case class Course (
  id: Option[Int],
  title: Option[String],
  description: Option[String],
  image: Option[String],
  price: Option[BigDecimal],
  duration: Option[String]
)

