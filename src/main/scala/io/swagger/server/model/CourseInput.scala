package io.swagger.server.model

import java.math.BigDecimal

/**
 * @param title Название курса
 * @param description Описание курса
 * @param image URL изображения для курса
 * @param price Цена курса
 * @param duration Длительность курса
 */
case class CourseInput (
  title: Option[String],
  description: Option[String],
  image: Option[String],
  price: Option[BigDecimal],
  duration: Option[String]
)

