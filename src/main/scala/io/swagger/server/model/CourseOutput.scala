package io.swagger.server.model


/**
 * @param id Идентификатор курса
 * @param name Название курса
 * @param description Описание курса
 * @param teacher Преподаватель курса
 */
case class CourseOutput (
  id: String,
  name: String,
  description: Option[String],
  teacher: String
)

