package io.swagger.server.model


/**
 * @param course 
 */
case class CourseResponse (
  course: Option[Course]
)

