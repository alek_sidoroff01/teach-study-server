package io.swagger.server.model


/**
 * @param courses 
 * @param total Общее количество курсов
 * @param page Номер текущей страницы
 * @param limit Количество элементов на странице
 */
case class CoursesListResponse (
  courses: Option[List[Course]],
  total: Option[Int],
  page: Option[Int],
  limit: Option[Int]
)

