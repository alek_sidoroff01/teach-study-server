package io.swagger.server.model


/**
 * @param message Описание ошибки. for example: ''Курс не найден.''
 */
case class Error (
  message: Option[String]
)

