package io.swagger.server.model


/**
 * @param error Описание ошибки
 */
case class ErrorResponse (
  error: String
)

