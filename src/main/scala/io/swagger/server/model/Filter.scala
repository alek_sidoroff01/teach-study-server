package io.swagger.server.model


/**
 * @param name Фильтр по имени пользователя. for example: ''John Doe''
 * @param email Фильтр по электронной почте пользователя. for example: ''johndoe@example.com''
 */
case class Filter (
  name: Option[String],
  email: Option[String]
)

