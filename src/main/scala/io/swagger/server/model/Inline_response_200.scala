package io.swagger.server.model


/**
 * @param tests 
 * @param total_count Общее количество тестов
 * @param page Номер текущей страницы
 * @param page_count Количество страниц
 */
case class Inline_response_200 (
  tests: List[Test],
  total_count: Int,
  page: Int,
  page_count: Int
)

