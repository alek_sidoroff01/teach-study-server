package io.swagger.server.model


/**
 * @param course_progress Процент пройденных уроков в курсе for example: ''80''
 * @param test_results 
 */
case class Inline_response_200_1 (
  course_progress: Option[Int],
  test_results: Option[List[inline_response_200_1_test_results]]
)

