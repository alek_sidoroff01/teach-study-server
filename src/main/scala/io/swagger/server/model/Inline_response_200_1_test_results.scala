package io.swagger.server.model


/**
 * @param test_id Идентификатор теста for example: ''1''
 * @param test_name Название теста for example: ''Тест по теме 1''
 * @param result Результат теста в процентах for example: ''90''
 */
case class Inline_response_200_1_test_results (
  test_id: Option[Int],
  test_name: Option[String],
  result: Option[Int]
)

