package io.swagger.server.model


/**
 * @param id Идентификатор урока
 * @param name Название урока
 * @param description Описание урока
 * @param duration Продолжительность урока (в минутах)
 * @param video_url Ссылка на видеоурок
 * @param materials_url Ссылка на материалы урока
 */
case class Lesson (
  id: Option[Int],
  name: Option[String],
  description: Option[String],
  duration: Option[Int],
  video_url: Option[String],
  materials_url: Option[String]
)

