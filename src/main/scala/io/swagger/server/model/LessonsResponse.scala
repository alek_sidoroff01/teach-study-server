package io.swagger.server.model


/**
 * @param lessons 
 * @param total Общее количество уроков в курсе
 */
case class LessonsResponse (
  lessons: Option[List[Lesson]],
  total: Option[Int]
)

