package io.swagger.server.model


/**
 * @param username Имя пользователя
 * @param password Пароль пользователя
 */
case class LoginCredentials (
  username: String,
  password: String
)

