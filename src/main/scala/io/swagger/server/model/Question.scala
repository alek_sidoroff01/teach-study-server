package io.swagger.server.model


/**
 * @param id Идентификатор вопроса
 * @param text Текст вопроса
 * @param choices Список вариантов ответов на вопрос
 */
case class Question (
  id: Int,
  text: String,
  choices: List[Question_choices]
)

