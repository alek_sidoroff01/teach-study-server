package io.swagger.server.model


/**
 * @param id Идентификатор ответа
 * @param text Текст ответа
 * @param is_correct Флаг, указывающий на правильность ответа
 */
case class Question_choices (
  id: Option[Int],
  text: Option[String],
  is_correct: Option[Boolean]
)

