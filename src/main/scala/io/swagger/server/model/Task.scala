package io.swagger.server.model

import java.util.Date

/**
 * @param id Идентификатор задания
 * @param name Название задания
 * @param description Описание задания
 * @param deadline Крайний срок выполнения задания
 */
case class Task (
  id: Int,
  name: String,
  description: String,
  deadline: Date
)

