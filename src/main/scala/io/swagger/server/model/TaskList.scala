package io.swagger.server.model


/**
 * @param tasks Список заданий
 * @param page Номер текущей страницы
 * @param total_pages Общее количество страниц
 * @param total_items Общее количество элементов
 */
case class TaskList (
  tasks: List[Task],
  page: Int,
  total_pages: Int,
  total_items: Int
)

