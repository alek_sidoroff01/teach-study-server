package io.swagger.server.model


/**
 * @param id Идентификатор теста
 * @param name Название теста
 * @param description Описание теста
 * @param questions Список вопросов теста
 */
case class Test (
  id: Int,
  name: String,
  description: Option[String],
  questions: List[Question]
)

