package io.swagger.server.model


/**
 * @param user_id Идентификатор пользователя
 * @param answers Список ответов на вопросы теста
 */
case class TestResult (
  user_id: Int,
  answers: List[Answer]
)

