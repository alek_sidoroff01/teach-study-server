package io.swagger.server.model


/**
 * @param username Имя пользователя
 * @param password Пароль пользователя
 * @param email Email пользователя
 */
case class User (
  username: String,
  password: String,
  email: String
)

